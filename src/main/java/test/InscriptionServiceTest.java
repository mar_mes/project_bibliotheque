package test;


import static org.junit.Assert.assertEquals;
import org.junit.Test;

import inscription.modele.Inscription;
import inscription.modele.InscriptionInvalideException;
import inscription.modele.InscriptionService;

public class InscriptionServiceTest {
	
	private InscriptionService inscriptionService = new InscriptionService();
	
	@Test
	public final void testInscrireEmail() throws InscriptionInvalideException {
		Inscription test = new Inscription("test@test.fr", "testtest");
		Inscription result = inscriptionService.inscrire("test@test.fr", "testtest", "testtest", true);
		assertEquals(test.getEmail(), result.getEmail());
	}
	
	@Test
	public final void testInscrireMotDePasse() throws InscriptionInvalideException {
		Inscription test = new Inscription("test@test.fr", "testtest");
		Inscription result = inscriptionService.inscrire("test@test.fr", "testtest", "testtest", true);
		assertEquals(test.getMotDePasse(), result.getMotDePasse());
	}
	
	@Test
	public final void testInscrireDate() throws InscriptionInvalideException {
		Inscription test = new Inscription("test@test.fr", "testtest");
		Inscription result = inscriptionService.inscrire("test@test.fr", "testtest", "testtest", true);
		assertEquals(test.getDate(), result.getDate());
	}
}